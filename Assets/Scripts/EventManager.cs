﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ReceivesEvents : MonoBehaviour {

}

public class EventManager : MonoBehaviour
{
    private static EventManager _instance;

    public static EventManager Instance {
        get {
            return _instance ?? (_instance = new EventManager());
        }
    }

    void Awake() {
        var recievesEvents = FindObjectsOfType<ReceivesEvents>();
        foreach (var recievesEvent in recievesEvents) {
            var recieverType = recievesEvent.GetType();
            //recieverType.GetInterfaces().Where(x => x.Name == "")
            foreach(var interfaceType in recieverType.GetInterfaces()
                .Where(x => x.IsGenericType && x.IsAssignableFrom(typeof(IEventHandler<>))))
            {
                // get generic types -> these will be the event types
                // register object for each event type
            }
        }
    }
    
    Dictionary<Type, List<Action<IEvent>>> eventDictionary = new Dictionary<Type, List<Action<IEvent>>>();

    Dictionary<Type, List<object>> otherEventDictionary = new Dictionary<Type, List<object>>();

    // Register the listener for an event.
    public void Register(Type eventType, Action<IEvent> eventCallback) {

        if (!eventType.IsAssignableFrom(eventType)) throw new Exception("Needs to implement IEvent");

        if (eventDictionary.ContainsKey(eventType)) {
            var callbackList = eventDictionary[eventType];
            callbackList.Add(eventCallback);
        }
        else {
            var callbackList = new List<Action<IEvent>>();
            callbackList.Add(eventCallback);
            eventDictionary.Add(eventType, callbackList);
        }
    }

    // Register the listener for an event.
    public void Register<T>(Action<IEvent> eventCallback) where T : IEvent {
        Register(typeof(T), eventCallback);
    }



    public void RegisterThing<T>(IEventHandler<T> eventHandler) where T : IEvent {
        var t = new Dictionary<Type, List<IEventHandler<T>>>();
        var eventType = typeof(T);

        if (otherEventDictionary.ContainsKey(eventType)) {
            var callbackList = otherEventDictionary[eventType];
            callbackList.Add(eventHandler);
        }
        else {
            var callbackList = new List<object>();
            callbackList.Add(eventHandler);
            otherEventDictionary.Add(eventType, callbackList);
        }
    }

    public void RaiseThing<T>(T @event) where T : IEvent {
        var callbackList = otherEventDictionary[typeof(T)];
        var typeCallbacks = callbackList.OfType<IEventHandler<T>>();
        foreach (var callback in typeCallbacks) {
            callback?.Handle(@event);
        }
    }

    //public void Register(Type eventType, IEventHandler<> handler) {

        //}

        // Raise the event.
    public void Raise<T>(T eventValue) where T : IEvent {
        var type = typeof(T);

        if (eventDictionary.ContainsKey(type)) {
            
            var callbackList = eventDictionary[type];
            foreach (var callback in callbackList) {
                callback?.Invoke(eventValue);
            }
        }
    }

    public void RemoveEvent() {

    }
}
