﻿using System.Collections;
using System.Collections.Generic;


public interface IEvent
{

}

public interface IEventHandler<T> where T : IEvent {
    void Handle(T @event);
}

public class SimpleEvent : IEvent {
    public string StringProperty { get; set; }
    public int IntProperty { get; set; }
}

public class HelloWorldEvent : IEvent{
    public string HelloWorldString { get; set; }
}