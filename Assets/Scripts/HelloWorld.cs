﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class HelloWorld : MonoBehaviour,
    IEventHandler<HelloWorldEvent> {

    public void RegisterHelloWorldEvent() {
        Debug.Log("Register Hello World Event");
        EventManager.Instance.Register<HelloWorldEvent>(PrintHelloWorld);
    }

    public void RegisterThingHelloWorldEvent() {
        Debug.Log("RegisterThing Hello World Event");
        EventManager.Instance.RegisterThing(this);
    }

    public void PrintHelloWorld(IEvent @event) {
        if (@event is HelloWorldEvent helloWorldEvent) {
            Debug.Log("HelloWorld");
        }
    }

    public void RaiseHelloWorldEvent() {
        Debug.Log("Raise Hello World Event");
        EventManager.Instance.Raise(new HelloWorldEvent());
    }

    public void RaiseThingHelloWorldEvent() {
        Debug.Log("Raise Hello World Event");
        EventManager.Instance.RaiseThing(new HelloWorldEvent());
    }

    public void Handle(HelloWorldEvent @event) {
        Debug.Log("Handle RegisterThing Event");
        Debug.Log("Hello World!");
    }
}
